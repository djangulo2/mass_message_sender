import os

from bs4 import BeautifulSoup
from requests import session
from openpyxl import load_workbook, Workbook

ABC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

class MessageSender(object):
    """Class to handle logging into EIS and send messages via the
    requests library."""
    session = None
    workbook = None
    eis_sheet = None
    recipients = None

    def __init__(self, *args, **kwargs):
        try:
            self.workbook = load_workbook(filename='mass_sender.xlsm',
                                          keep_vba=True, read_only=False)
            if 'EIS' in self.workbook.sheetnames:
                self.eis_sheet = self.workbook['EIS']
        except FileNotFoundError:
            pass
            # self.workbook = Workbook()


    def eis_login(self, eis_id=None, password=None):
        """Logs the MessageSender.session object into the system."""
        os.system('SET HTTPS_PROXY=cutprx.alorica.com:3128')
        os.system('SET HTTP_PROXY=cutprx.alorica.com:3128')
        login_payload = {
            'ID': eis_id,
            'Password': password,
            'w': os.environ['COMPUTERNAME'],
            'u': os.environ['USERNAME'],
        }
        self.session.post('https://eis.alorica.com/Login.asp',
                          data=login_payload)
    
    def get_eis_headcount(self):
        htlm_headcount = self.session.get(
            'https://eis.alorica.com/Reporting/HeadCountAll/' \
            'Account.asp?Account=Main'
        )
        for i, j in htlm_headcount.items():
            print('{}: {}'.format(i, j))
        soup_headcount = BeautifulSoup(htlm_headcount.text, 'html.parser')
        headcount = soup_headcount.find('table',
            {
                'style': 'border: 1px solid #c7c7c7;font-family: arial; fon' \
                't-size: 12 pt;display:block;'
            })
        headcount_list = list()
        for unit in headcount.findAll('td'):
            hc_list.append(unit.string.strip())
        names = list()
        for name in headcount.findAll('u'):
            names.append(name.string.strip())

        self.list_to_table(workbook=self.workbook, worksheet_name='EIS',
                           columns=13, headers=True, vector=headcount_list)
        self.list_to_table(workbook=self.workbook, worksheet_name='EIS',
                           tgt_row=2, tgt_col=2, columns=1, vector=names,
                           headers=True)

    def get_recipients_dict(self):
        html = self.session.get(
            'https://eis.alorica.com/Reporting/EIS/' \
            'Messaging/Compose.asp')
        soup = BeautifulSoup(html.text, 'htlm.parser')
        agents = [soup.find('select', {'name': 'Agent'}).findAll('option')]



    def list_to_table(self, workbook, worksheet_name=None, tgt_row=1,
                      tgt_col=1, columns=None, headers=False, vector=None):
        row = tgt_row
        col = tgt_col
        ws = workbook[worksheet_name]
        try:
            iter(vector)
        except TypeError:
            raise TypeError('{} is not iterable'.format(type(vector)))
        if headers:
            for i in range(len(vector)):
                ws.cell(row=row, column=col, value=vector[i])
                col += 1
                if (i + 1) % columns == 0:
                    row += 1
                    col = tgt_col
        else:
            for i in range(columns, len(vector)):
                ws.cell(row=row, column=col, value=vector[i])
                col += 1
                if (i + 1) % columns == 0:
                    row += 1
                    col = tgt_col
                


        
        
